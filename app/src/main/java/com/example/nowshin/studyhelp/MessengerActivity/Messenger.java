package com.example.nowshin.studyhelp.MessengerActivity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.example.nowshin.studyhelp.R;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class Messenger extends AppCompatActivity {

    private DatabaseReference mydatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);

        mydatabase= FirebaseDatabase.getInstance().getReference("Message");

        final TextView myText= findViewById(R.id.textbox);

        mydatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String[] Messages= dataSnapshot.getValue().toString().split(",");

                myText.setText("");

                for(int i=0;i<Messages.length;i++)
                {
                    String[] finalMsg= Messages[i].split("=");
                    myText.append(finalMsg[1]+ "\n");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                myText.setText("CANCELLED!");
            }
        });
    }

    public void sendMessage(View view){
        EditText myEdittext= findViewById(R.id.editText);

        mydatabase.child(Long.toString(System.currentTimeMillis())).setValue(myEdittext.getText().toString());
        myEdittext.setText("");


    }
}
