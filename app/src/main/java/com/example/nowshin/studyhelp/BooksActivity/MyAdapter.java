package com.example.nowshin.studyhelp.BooksActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.nowshin.studyhelp.R;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    List<Listitem> listitems;
    public Context context;

    public MyAdapter(List<Listitem> listitems, Context context) {
        this.listitems = listitems;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Listitem listitem=listitems.get(position);


        holder.textViewHead.setText(listitem.getHead());
        //holder.textViewDesc.setText(listitem.getDesc());
        // final String url=listitem.getDesc();
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "text", Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, "Hey I'm a toast messsage",
                //   Toast.LENGTH_LONG).show();
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(listitem.getDesc()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textViewHead;
        // public TextView textViewDesc;
        public Button button;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewHead=(TextView) itemView.findViewById(R.id.textViewHead);
            //textViewDesc=(TextView) itemView.findViewById(R.id.textViewDesc);
            button=(Button)itemView.findViewById(R.id.button);
        }
    }

}
