package com.example.nowshin.studyhelp.CourseActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.nowshin.studyhelp.R;

public class Courses extends AppCompatActivity {

    private Button ml,ai,dl,java,python,c,cpp,dsa,prob,asmbl,ds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);

        ml=(Button) findViewById(R.id.ml);
        ai=(Button) findViewById(R.id.ai);
        dl=(Button) findViewById(R.id.dl);
        java=(Button) findViewById(R.id.java);
        python=(Button) findViewById(R.id.python);
        c=(Button) findViewById(R.id.c);
        cpp=(Button) findViewById(R.id.cpp);
        dsa=(Button) findViewById(R.id.dsa);
        ds=(Button) findViewById(R.id.ds);
        asmbl=(Button) findViewById(R.id.asmbl);
        prob=(Button) findViewById(R.id.probability);

        ml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,ML.class));
            }
        });

        ai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,AI.class));
            }
        });

        dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,dl.class));
            }
        });

        java.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Java.class));
            }
        });

        python.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Python.class));
            }
        });

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,C.class));
            }
        });

        cpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Cpp.class));
            }
        });

        dsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Dsa.class));
            }
        });

        prob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Probability.class));
            }
        });

        ds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Ds.class));
            }
        });

        asmbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Courses.this,Assembly.class));
            }
        });

    }
}
