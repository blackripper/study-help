package com.example.nowshin.studyhelp.BooksActivity;

import android.widget.Button;

public class Listitem {
    private String head;
    private  String desc;
    //private Button button;

    public Listitem(String head, String desc) {
        this.head = head;
        this.desc = desc;
        // this.button=button;
    }

    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }

    // public Button getButton() {
    //     return button;
    // }
}
